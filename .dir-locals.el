((scheme-mode
  . ((indent-tabs-mode . nil)
     (eval . (put 'stream-let 'scheme-indent-function 2))
     (eval . (put 'call-with-input-command 'scheme-indent-function 1))
     (eval . (put 'call-with-output-command 'scheme-indent-function 1))
     (eval . (put 'with-input-from-command 'scheme-indent-function 1))
     (eval . (put 'with-output-to-command 'scheme-indent-function 1)))))
