(define-module (grump display)
  #:export (display-each))

(define* (display-each items #:optional (port (current-output-port)))
  "Display each element of ITEMS on a separate line.  Output is sent to
PORT or the current output port if not given.  The return value is not
specified."
  (for-each (lambda (item)
              (display item port)
              (newline port))
            items))
