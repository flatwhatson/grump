(define-module (grump lists)
  #:use-module (srfi srfi-1)
  #:export (take-between))

(define (take-between start-pred end-pred ls)
  (let ((tail (drop-while (negate start-pred) ls)))
    (if (null? tail) tail
        (take-while (negate end-pred) (cdr tail)))))
