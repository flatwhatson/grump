(define-module (grump statistics)
  #:export (mean
            median))

(define (mean . vals)
  (/ (apply + vals) (length vals)))

(define (median . vals)
  (let* ((vals (sort vals <))
         (mid (/ (length vals) 2)))
    (if (integer? mid)
        (list-ref vals mid)
        (/ (+ (list-ref vals (floor mid))
              (list-ref vals (ceiling mid))) 2))))
