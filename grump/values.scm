(define-module (grump values)
  #:export (first-value))

(define-syntax-rule (first-value expr expr* ...)
  (call-with-values
      (lambda ()
        expr expr* ...)
    (lambda (result . _)
      result)))
