(define-module (grump vectors)
  #:use-module (srfi srfi-43)
  #:export (vector-find))

(define (vector-find pred? vec1 . vecn)
  (apply vector-any
         (lambda vals
           (and (apply pred? vals)
                (apply values vals)))
         vec1 vecn))
