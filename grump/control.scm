(define-module (grump control)
  #:use-module (ice-9 control)
  #:export (call-with-return))

(define-syntax call-with-return
  (syntax-rules ()
    ((_ thunk)
     (let ((tag (make-prompt-tag 'return)))
       (call-with-prompt tag
         (lambda ()
           (thunk (lambda (result)
                    (abort-to-prompt tag result))))
         (lambda (_ result)
           result))))))
