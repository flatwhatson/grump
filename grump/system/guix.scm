(define-module (grump system guix)
  #:use-module (grump lines)
  #:use-module (grump system)
  #:export (guix-executable
            guix-find-program))

(define-once guix-executable
  (make-parameter "guix"))

(define (guix-find-program program . packages)
  (call-with-input-command
      `(,(guix-executable) "shell" "--pure"
        "which" ,@packages "--"
        "which" ,program)
    next-line))
