(define-module (grump functional)
  #:use-module (srfi srfi-1)
  #:export (conjoin
            disjoin
            all-of
            any-of))

(define (conjoin . predicates)
  (lambda args
    (every (lambda (pred)
             (apply pred args)) predicates)))

(define (disjoin . predicates)
  (lambda args
    (any (lambda (pred)
           (apply pred args)) predicates)))

(define all-of conjoin)
(define any-of disjoin)
