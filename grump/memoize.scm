(define-module (grump memoize)
  #:export (lazily))

(define-syntax-rule (lazily expr expr* ...)
  (let ((promise (delay expr expr* ...)))
    (lambda ()
      (force promise))))
