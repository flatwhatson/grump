(define-module (grump strings)
  #:use-module (ice-9 regex)
  #:export (string-match-all))

(define* (string-match-all pattern str #:optional (n 0))
  (map (lambda (m)
         (match:substring m n))
       (list-matches pattern str)))
